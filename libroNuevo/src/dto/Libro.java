package dto;

public class Libro {
	
	//Aqu� creamos las variables y les asignamos tipo private
    private int ISBN;
    private String titulo;
    private String autor;
    private int numeroPaginas;
    
    //Aqu� creamos el constructor por defecto
    public Libro(){
        this.ISBN=0;
        this.titulo="";
        this.autor="";
        this.numeroPaginas=0;
    }
    
    //Aqu� creamos el constructor con los atributos correspondientes
    public Libro(int ISBN, String titulo, String autor, int numeroPaginas){
        this.ISBN=ISBN;
        this.titulo=titulo;
        this.autor=autor;
        this.numeroPaginas=numeroPaginas;
    }
    
    //Aqu� crearemos todos los getters y todos los setters necesarios
	public int getISBN() {
		return ISBN;
	}
	public void setISBN(int iSBN) {
		ISBN = iSBN;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public int getNumeroPaginas() {
		return numeroPaginas;
	}
	public void setNumeroPaginas(int numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}
	
	//Aqu� hacemos un toString para que se pueda printar por pantalla en una consola
	public String toString() {
		return "El libro con ISBN " + ISBN + " creado por el autor " + autor + " tiene " + numeroPaginas + " paginas.";
	}
}
