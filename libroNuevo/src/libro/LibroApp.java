package libro;
import dto.Libro;

public class LibroApp {

	public static void main(String[] args) {
		//Aqu� creo un objeto llamado libro1, referenciandolo al modelo libroNuevoDto
		Libro libro1 = new Libro(23753, "El jardin de la Luna", "Rey Misterio", 450);
		Libro libro2 = new Libro(61489, "El jardin del Sol", "Batista", 120);
		
		//Aqu� mostramos los objetos por consola
		System.out.println(libro1);
		System.out.println(libro2);
        
        //Comparamos quien tiene mas paginas
        if (libro1.getNumeroPaginas() > libro2.getNumeroPaginas()) {
            System.out.println(libro1.getTitulo() + " tiene mas paginas");
        } else {
            System.out.println(libro2.getTitulo() + " tiene mas paginas");
        }
	}

}
